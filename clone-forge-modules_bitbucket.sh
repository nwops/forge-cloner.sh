#!/usr/bin/env bash
# Purpose:  Mirrors the source git url to an auto generated git url to 
# an internal server.  Creates a new repo in the specified project if
# not already existed.
# Outputs a usable Puppetfile entry for git url
#  
BITBUCKET_BASE_URL='https://bitbucket.com'
BITBUCKET_API_URL="${BITBUCKET_BASE_URL}/rest/api/1.0"
PROJECT_ID='forge'
BITBUCKET=true
PROJECT_URL="${BITBUCKET_API_URL}/projects/${PROJECT_ID}/repos"
forge_modules=forge-modules.sh
repos=$(grep -vE '^(\s*$|#)' ${forge_modules})
base_dir=$PWD
# Export this variable with your Bitbucket token. Must have Admin rights to
# create repos in project "External Puppet Mods".
# Example: export BITBUCKET_TOKEN='<token here>'
if [[ -z BITBUCKET_TOKEN ]]; then
    echo "BITBUCKET_TOKEN environment variable is not defined"
    exit 1
fi

function create_repo {
    reponame=$1
    payload="{\"name\": \"${reponame}\",\"scmId\": \"git\", \"forkable\": true }"
    echo $payload > payload.json
    curl -k -s -H "Authorization: Bearer ${BITBUCKET_TOKEN}" \
         -H "Content-Type: application/json" \
         -d @payload.json \
         $PROJECT_URL

    rm -f payload.json
}
# clean up old cloned urls
rm -f "${base_dir}/cloned_urls.sh"

for mod_url in $repos; do
    mod_name=$(basename ${mod_url} .git)
    internal_url="${BITBUCKET_BASE_URL}/scm/${PROJECT_ID}/${mod_name}.git"
    mod_dir="${base_dir}/${mod_name}"
    cd $base_dir
    if [[ ! -d $mod_dir ]]; then
        # try and create the repo even if it already exists
        git clone --mirror $mod_url $mod_dir &> /dev/null
    fi
    # if this is a bitbucket server we need to create the repo first 
    # if the repo already exist the below will fail
    if [[ $BITBUCKET == 'true' ]]; then
        create_repo $mod_name
    fi
    # if the upstream repo directory the remote content may not be updated
    # delete dir and re-run
    echo 
    echo "Created mirror directory: ${mod_name}"
    if [[ -d $mod_dir ]]; then
        cd $mod_dir
        # seems to only work with ssh based auth when updating
        #git push --mirror $internal_url
        current_branch=$(git rev-parse --abbrev-ref HEAD)
        mod_function="mod '${mod_name}', \n\tgit: '${internal_url}',\n\tref: '${current_branch}'"
        echo -e $mod_function >> "${base_dir}/cloned_urls.sh"
        rm -rf $mod_dir
    fi
  
done

