# Forge Cloner
This is a simple script that will clone modules from the puppet forge and create new projects in the internal gitlab puppet/forge namespace.  Currently this only works locally and no CI pipeline has been created.  The name of the repo will be the same from the git url provided in forge-modules.sh

## Usage
1. `echo https://github.com/puppetlabs/puppetlabs-stdlib.git > forge-modules.sh`
2. `bash clone-forge-modules.sh`

If a forge-modules.sh file already exist just add new git urls
### Adding modules
To add a module to be mirrored you simply just add the git url to the forge-modules.sh file and then
run the clone-forge-modules.sh script.

### Updating modules
The procedure for updating modules is the same as adding modules.  Just run the clone-forge-modules.sh


### Note
* If a repo already exist in gitlab with the same name this script will overwrite that repository.  
* Mirroring to gitlab can take time if the incoming module has many tags and branches
