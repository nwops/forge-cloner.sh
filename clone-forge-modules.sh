#!/usr/bin/env bash
forge_modules=forge-modules.sh
server='git@gitlab.com'
namespace=nwops
repos=$(grep -vE '^(\s*$|#)' ${forge_modules})
base_dir=$PWD
for mod_url in $repos; do
    mod_name=$(basename ${mod_url})
    internal_url="${server}/${namespace}/${mod_name}"
 
    mod_dir="${base_dir}/${mod_name}"
    cd $base_dir
    if [[ ! -d $mod_dir ]]; then
        git clone --mirror $mod_url 
    fi
    # if the upstream repo directory the remote content may not be updated
    # delete dir and re-run
    cd $mod_dir
    # seems to only work with ssh auth when updating
    git push --mirror $internal_url
    rm -rf $mod_dir
done
